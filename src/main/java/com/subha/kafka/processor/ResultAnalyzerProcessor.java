package com.subha.kafka.processor;

import com.subha.kafka.domain.GeneratedValues;
import com.subha.kafka.domain.PredictedValue;
import com.subha.kafka.repositories.GeneratedValueRepository;
import com.subha.kafka.repositories.PredictedValuesRepository;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by subha on 09/07/2018.
 */
@Component("resultAnalyzer")
public class ResultAnalyzerProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(ResultAnalyzerProcessor.class);

    @Value("${game.generator.number}")
    private String numberOfGenerators;

    @Value("${game.player.number}")
    private String numberOfPlayers;

    @Value("${kafka.result-topic}")
    private String resultTopic;

    @Value("${kafka.server}")
    private String kafkaServer;

    @Value("${kafka.port}")
    private String kafkaPort;

    @Autowired
    private GeneratedValueRepository generatedValueRepository;

    @Autowired
    private PredictedValuesRepository predictedValuesRepository;

    @Autowired
    private ProducerTemplate producerTemplate;

   Set<String> gameSet = Collections.synchronizedSet(new HashSet<>());

    public void addGeneratedValues(Exchange exchange){
        GeneratedValues model = new GeneratedValues();
        model.setValue(Long.parseLong(exchange.getIn().getBody().toString().split("-")[0]));
        model.setGenerator(exchange.getIn().getBody().toString().split("-")[1]);
        model.setGame(exchange.getIn().getBody().toString().split("-")[2]);
        model.setRead("false");
        generatedValueRepository.save(model);
        gameSet.add(exchange.getIn().getBody().toString().split("-")[2]);
        LOG.info("Generated Data save" + model.getValue());
    }

    public void addPredictedValuesValues(Exchange exchange){
        PredictedValue model = new PredictedValue();
        model.setPredict(Long.parseLong(exchange.getIn().getBody().toString().split("-")[0]));
        model.setPlayer(exchange.getIn().getBody().toString().split("-")[1]);
        model.setGame(exchange.getIn().getBody().toString().split("-")[2]);
        model.setRead("false");
        predictedValuesRepository.save(model);
        gameSet.add(exchange.getIn().getBody().toString().split("-")[2]);
        LOG.info("Predicted Data save for " + model.getPlayer() + "predicted number " + model.getPredict());
    }

    public void analyzeResult(Exchange exchange){

        Pageable pageableGen = new PageRequest(0,Integer.parseInt(numberOfGenerators));
        Pageable pageablePredict = new PageRequest(0,Integer.parseInt(numberOfPlayers));

        String gameId = gameSet.iterator().hasNext() ? gameSet.iterator().next() : null;
        List<GeneratedValues> genList = null;

        List<PredictedValue> predictList =  null;

        if(gameId!= null){
            genList =  generatedValueRepository.allUnreadValues(gameId, pageableGen);
            predictList =  predictedValuesRepository.allUnreadValues(gameId, pageablePredict);
        }


        StringBuilder result = new StringBuilder("\n\r --Game : " + gameId +"-- \n\rPlayer    Score\n\r");
        if(genList !=null && genList.size() >= Integer.parseInt(numberOfGenerators) && predictList!= null && predictList.size() >= Integer.parseInt(numberOfPlayers)){
            LOG.info("Generated value size = "+ genList.size());
            LOG.info("Predicted value size = "+ predictList.size());
            for (PredictedValue predict : predictList){
                long predictedValue = predict.getPredict();
                String player = predict.getPlayer();
                long score = genList.stream().map(generatedValues -> generatedValues.getValue()).filter(value -> value == predictedValue).count();
                result.append(player).append("  ").append(score).append("\n\r");
                predict.setRead("true");
                predictedValuesRepository.save(predict);
            }
            result.append("--------------------------------------");
            genList.forEach(generatedValue -> {
                generatedValue.setRead("true");
                generatedValueRepository.save(generatedValue);
            });

            producerTemplate.sendBody("kafka:"+ resultTopic + "?brokers=" + kafkaServer + ":" + kafkaPort, result.toString());
            gameSet.remove(gameId);
        }

    }

}
