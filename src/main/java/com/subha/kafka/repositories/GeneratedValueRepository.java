package com.subha.kafka.repositories;

import com.subha.kafka.domain.GeneratedValues;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by subha on 09/07/2018.
 */
public interface GeneratedValueRepository extends CrudRepository<GeneratedValues, Long> {

    @Query("from GeneratedValues where is_read = 'false' and game=:gameId order by id asc")
    List<GeneratedValues> allUnreadValues(@Param("gameId") String gameId, Pageable pageable);
}
