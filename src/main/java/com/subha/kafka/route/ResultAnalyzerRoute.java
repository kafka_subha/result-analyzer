package com.subha.kafka.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 09/07/2018.
 */

@Component
public class ResultAnalyzerRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:resultAnalyzer?level=ERROR&showHeaders=true&showBody=true&showCaughtException=true&showStackTrace=true")
                .useOriginalMessage());

        from("kafka:{{kafka.generator-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}_gen&autoOffsetReset=earliest&consumersCount=1")
                .routeId("collect-generated-value")
                .to("bean:resultAnalyzer?method=addGeneratedValues");

        from("kafka:{{kafka.player-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}_predict&autoOffsetReset=earliest&consumersCount=1")
                .routeId("collect-guess-value")
                .to("bean:resultAnalyzer?method=addPredictedValuesValues");

        from("timer:result-analyzer?period=500")
                .routeId("analyzer")
                .to("bean:resultAnalyzer?method=analyzeResult");
    }
}
