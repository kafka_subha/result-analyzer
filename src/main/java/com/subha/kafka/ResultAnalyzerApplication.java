package com.subha.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by subha on 09/07/2018.
 */

@SpringBootApplication
public class ResultAnalyzerApplication {

    public static void main(String... args){
        SpringApplication.run(ResultAnalyzerApplication.class, args);
        while (true){
            //noop
        }
    }
}
